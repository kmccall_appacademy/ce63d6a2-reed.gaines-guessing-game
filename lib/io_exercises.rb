# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  answer = (1..100).to_a.sample
  counter = 0
  guesses = 0
  while counter < 1
    puts "Guess a number."
    $stdout.flush
    guess = gets.to_i
    if guess < answer
      guesses += 1
      puts "Your guess was #{guess}."
      puts "That's too low. Try again."
      puts "You've made #{guesses} guesses."
    elsif guess > answer
      guesses += 1
      puts "Your guess was #{guess}."
      puts "That's too high. Try again."
      puts "You've made #{guesses} guesses."
    else
      counter += 1
      guesses += 1
      puts "Your guess was #{guess}."
      puts "Congratulations! Precisely! It's #{answer}!"
      puts "You got it in only #{guesses} guesses!"
    end
  end
end
